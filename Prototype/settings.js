window.onload = () => {

  //switchButtons
  const aanBtnSettings1 = document.getElementById('on_settings1');
  const uitBtnSettings1 = document.getElementById('off_settings1');

  const aanBtnSettings2 = document.getElementById('on_settings2');
  const uitBtnSettings2 = document.getElementById('off_settings2');

  const aanBtnSettings3 = document.getElementById('on_settings3');
  const uitBtnSettings3 = document.getElementById('off_settings3');

  //fixt veranderen maximale temperatuur
  var maxTemp = document.getElementById('temperatuurparagraaf');
  var verhoogtemp = document.getElementById('highertemp');
  var verlaagtemp = document.getElementById('lowertemp');
  var standaardTemp = 23;
  var gradencel = "C";

  //fixt veranderen luchtvochtigheidparagraaf
  var maxLuchtvochtigheid = document.getElementById("js--luchtvochtigheidparagraaf");
  var verhoogLuchtvochtigheid = document.getElementById('higherpercentage');
  var verlaagLuchtvochtigheid = document.getElementById('lowerpercentage');
  var standaardLuchtvochtigheid = 40;
  var percentage = "%";

  //audio

  const audio = new Audio("geluid.wav");
  const aanUit = new Audio('aanuit.wav');

//main
  switchButtons(aanBtnSettings1,uitBtnSettings1);
  switchButtons(aanBtnSettings2,uitBtnSettings2);
  switchButtons(aanBtnSettings3,uitBtnSettings3);


  function switchButtons(button1, button2){
    button1.onclick = ()=> {
      console.log("aan");
      button1.classList.remove("deselected");
      button1.classList.add("selected");
      button2.classList.remove("selected");
      button2.classList.add("deselected");
      aanUit.play();

    }
    button2.onclick = ()=> {
      console.log("uit");
      button1.classList.remove("selected");
      button1.classList.add("deselected");
      button2.classList.remove("deselected");
      button2.classList.add("selected");
      aanUit.play();
    }
  }

  verhoogtemp.onclick = ()=> {
    standaardTemp++;
    maxTemp.innerHTML = standaardTemp + gradencel;
    audio.play();
  }

  verlaagtemp.onclick = ()=> {
    standaardTemp--;
    maxTemp.innerHTML = standaardTemp + gradencel;
    audio.play();
  }

  verhoogLuchtvochtigheid.onclick = ()=>{
    standaardLuchtvochtigheid++;
    maxLuchtvochtigheid.innerHTML = standaardLuchtvochtigheid + percentage;
    audio.play();
  }

  verlaagLuchtvochtigheid.onclick = ()=>{
    standaardLuchtvochtigheid--;
    maxLuchtvochtigheid.innerHTML = standaardLuchtvochtigheid + percentage;
    audio.play();
  }


}
