window.onload = ()=>{

//aan en uit buttons per pagina
  const aanBtnHome = document.getElementById('on_home');
  const uitBtnHome = document.getElementById('off_home');

//Audio
const aanUit = new Audio('aanuit.wav');

//standen

const stand1 = document.getElementById('speed1');
const stand2 = document.getElementById('speed2');
const stand3 = document.getElementById('speed3');



switchButtons(aanBtnHome,uitBtnHome);
switchStanden();


function switchButtons(button1, button2){
  button1.onclick = ()=> {
    console.log("aan");
    button1.classList.remove("deselected");
    button1.classList.add("selected");
    button2.classList.remove("selected");
    button2.classList.add("deselected");
    aanUit.play();
  }
  button2.onclick = ()=> {
    console.log("uit");
    button1.classList.remove("selected");
    button1.classList.add("deselected");
    button2.classList.remove("deselected");
    button2.classList.add("selected");
    aanUit.play();
  }
}

function switchStanden(){
  stand1.onclick = ()=>{
    stand1.classList.remove("deselected");
    stand1.classList.add("selected");
    stand2.classList.add("deselected");
    stand3.classList.add("deselected");

  }

  stand2.onclick = ()=>{
    stand2.classList.remove("deselected");
    stand2.classList.add("selected");
    stand1.classList.add("deselected");
    stand3.classList.add("deselected");

  }

  stand3.onclick = ()=>{
    stand3.classList.remove("deselected");
    stand3.classList.add("selected");
    stand2.classList.add("deselected");
    stand1.classList.add("deselected");

  }
}
}
